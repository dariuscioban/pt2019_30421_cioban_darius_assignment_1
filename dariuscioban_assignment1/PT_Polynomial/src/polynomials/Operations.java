package polynomials;

import java.util.*;

public class Operations {

	public static Polynomial add(Polynomial p1, Polynomial p2) {
		Polynomial result = new Polynomial();
		for(Monomial i : p1.monoList) {
			Monomial aux = new Monomial(i.getCoef(), i.getExp());
			result.monoList.add(aux);
		}
		for(Monomial i : p2.monoList) {
			if(!checkIfExists(result.monoList, i)) {
				Monomial aux = new Monomial(i.getCoef(), i.getExp());
				result.monoList.add(aux);
			}
		}
		return result;
	}
	
	public static Polynomial subtract(Polynomial p1, Polynomial p2) {
		Polynomial result = new Polynomial();
		for(Monomial i : p2.monoList) {
			Monomial aux = new Monomial(-i.getCoef(), i.getExp());
			result.monoList.add(aux);
		}
		for(Monomial i : p1.monoList) {
			if(!checkIfExists(result.monoList, i)) {
				Monomial aux = new Monomial(i.getCoef(), i.getExp());
				result.monoList.add(aux);
			}
		}
		return result;
	}
	
	public static Polynomial multiply(Polynomial p1, Polynomial p2) {
		Polynomial result = new Polynomial();
		for(Monomial i : p1.monoList) {
			for(Monomial j : p2.monoList) {
				Monomial aux = new Monomial(i.getCoef() * j.getCoef(), i.getExp() + j.getExp());
				if(!checkIfExists(result.monoList, aux))
					result.monoList.add(aux);
			}
		}
		return result;
	}
	
	public static Polynomial[] divide(Polynomial p1, Polynomial p2) {
		Polynomial[] result = new Polynomial [2];
		Polynomial quotient = new Polynomial();
		Polynomial pCopy = copy(p1);
		Monomial firstPCopy = pCopy.monoList.get(0);
		Monomial firstP2 = p2.monoList.get(0);
		while(firstPCopy.getExp() >= firstP2.getExp()) {
			Monomial aux = new Monomial(firstPCopy.getCoef() / firstP2.getCoef(), firstPCopy.getExp() - firstP2.getExp());
			Polynomial auxPoly = new Polynomial();
			auxPoly.monoList.add(aux);
			quotient.monoList.add(aux);
			pCopy = subtract(pCopy, multiply(p2, auxPoly));
			pCopy.toString(); // we use the toString method just to actualize (get rid of monomials with coefficient 0)
			if(!pCopy.monoList.isEmpty()) {
				firstPCopy = pCopy.monoList.get(0);
			} else {
				firstPCopy = new Monomial(0, -1);
			}
		}
		result[0] = quotient;
		result[1] = pCopy;
		return result;
	}
	
	private static boolean checkIfExists(List<Monomial> list, Monomial mono) {
		boolean check = false;
		for(Monomial i : list) {
			if(i.getExp() == mono.getExp()) {
				i.setCoef(i.getCoef() + mono.getCoef());
				check = true;
			}
		}
		return check;
	}
	
	private static Polynomial copy(Polynomial p) {
		Polynomial result = new Polynomial();
		for(Monomial i : p.monoList) {
			Monomial aux = new Monomial(i.getCoef(), i.getExp());
			result.monoList.add(aux);
		}
		return result;
	}
}
