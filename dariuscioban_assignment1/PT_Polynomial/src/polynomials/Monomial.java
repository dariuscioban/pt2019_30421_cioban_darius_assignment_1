package polynomials;

public class Monomial implements Comparable<Monomial> {
	
	private int exp; //exponent of the monomial
	private double coef; //coefficient of the monomial
	
	//constructor
	public Monomial(double coefficient, int exponent) {
		exp = exponent;
		coef = coefficient;
	}
	
	//getters and setters
	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}
	
	public int compareTo(Monomial other) {
		 return other.getExp() - this.exp;
	}
	
}
