package polynomials;

import java.util.*;

public class Polynomial {

	public List<Monomial> monoList = new ArrayList<>();
	
	public void addMonomial(Monomial mono) {
		for(Monomial i : monoList)
			if(i.getExp() == mono.getExp()) {
				System.out.println("There already exists a monomial with that exponent in the polynomial!");
				return;
			}
		monoList.add(mono);
		System.out.println("Monomial added successfully!");
	}
	
	public Polynomial differentiate() {
		Polynomial result = new Polynomial();
		for(Monomial i : monoList) {
			Monomial aux = new Monomial(i.getCoef(), i.getExp());
			result.addMonomial(aux);
			
		}
		for(Monomial i : result.monoList) {
			i.setCoef(i.getCoef() * i.getExp());
			i.setExp(i.getExp() - 1);
		}
		result.actualize();
		return result;
	}
	
	public Polynomial integrate() {
		Polynomial result = new Polynomial();
		for(Monomial i : monoList) {
			Monomial aux = new Monomial(i.getCoef(), i.getExp());
			result.addMonomial(aux);
			
		}
		for(Monomial i : result.monoList) {
			i.setCoef(i.getCoef() / (i.getExp() + 1));
			i.setExp(i.getExp() + 1);
		}
		result.actualize();
		return result;
	}
	
	public String toString() {
		actualize();
		if(monoList.isEmpty()) {
			return "0";
		}
		String strRep = "";
		for(Monomial i : monoList) {
			int intCoef = (int) i.getCoef();
			if(i.getCoef() > 0)
				strRep += "+";
			if(i.getExp() == 0) {
				if(i.getCoef() == intCoef)
					strRep += intCoef;
				else strRep += i.getCoef();
			}
			else {
				if(i.getCoef() != 1 ) {
					if(i.getCoef() == intCoef)
						strRep += intCoef;
					else strRep += i.getCoef();
				}
				strRep += "X";
				if(i.getExp() != 1)
					strRep += "^" + i.getExp();
			}
			strRep += " ";
		}
		if(strRep.startsWith("+"))
			strRep = strRep.substring(1);
		return strRep;
	}
	
	private void actualize() {
		Collections.sort(monoList);
		monoList.removeIf(n -> (n.getCoef() == 0));
	}
	
}
