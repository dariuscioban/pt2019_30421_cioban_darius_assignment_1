package gui;
import polynomials.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class GraphicalUserInterface {
	
	Polynomial poly1;
	Polynomial poly2;
	Polynomial polyResult;
	Polynomial polyRemainder;
	
	JFrame frame = new JFrame("Polynomial Calculator");
	JTextField resField1 = new JTextField("		Result shown here		"); // 2 tabs both left and right
	JTextField resField2 = new JTextField("		Remainder shown here		"); // 2 tabs both left and right
	
	void initUI() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1200, 600);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel polyPanel = new JPanel();
		JPanel opPanel = new JPanel();
		opPanel.setLayout(new GridLayout(2, 4));
		JPanel resPanel = new JPanel();
		polyPanel.setLayout(new BoxLayout(polyPanel, BoxLayout.X_AXIS));
		JPanel polyPanel1 = new JPanel();
		JPanel polyPanel2 = new JPanel();
		JLabel polyLabel1 = new JLabel("Polynomial 1:");
		JLabel polyLabel2 = new JLabel("Polynomial 2:");
		JTextField polyField1 = new JTextField("	Enter the poylnomial here	"); //one tab
		JTextField polyField2 = new JTextField("	Enter the poylnomial here	"); //one tab
		JButton okBtn1 = new JButton("OK");
		JButton okBtn2 = new JButton("OK");
		JButton addBtn = new JButton("Add");
		JButton subBtn = new JButton("Subtract");
		JButton mulBtn = new JButton("Multiply");
		JButton divBtn = new JButton("Divide");
		JButton diffBtn1 = new JButton("Differentiate P1");
		JButton diffBtn2 = new JButton("Differentiate P2");
		JButton intBtn1 = new JButton("Integrate P1");
		JButton intBtn2 = new JButton("Integrate P2");
		JLabel resLabel1 = new JLabel("Result");	
		JLabel resLabel2 = new JLabel("Remainder");
		
		polyField1.addFocusListener(new FieldClicker(polyField1));
		polyField2.addFocusListener(new FieldClicker(polyField2));
		okBtn1.addActionListener(new ParseClicker(polyField1, 1));
		okBtn2.addActionListener(new ParseClicker(polyField2, 2));
		addBtn.addActionListener(new OpClicker(addBtn));
		subBtn.addActionListener(new OpClicker(subBtn));
		mulBtn.addActionListener(new OpClicker(mulBtn));
		divBtn.addActionListener(new OpClicker(divBtn));
		diffBtn1.addActionListener(new OpClicker(diffBtn1));
		diffBtn2.addActionListener(new OpClicker(diffBtn2));
		intBtn1.addActionListener(new OpClicker(intBtn1));
		intBtn2.addActionListener(new OpClicker(intBtn2));
		 	
		polyPanel1.add(polyLabel1);
		polyPanel1.add(polyField1);
		polyPanel1.add(okBtn1);
		polyPanel2.add(polyLabel2);
		polyPanel2.add(polyField2);
		polyPanel2.add(okBtn2);
		polyPanel.add(polyPanel1);
		polyPanel.add(Box.createRigidArea(new Dimension(0,5)) );
		polyPanel.add(polyPanel2);
		opPanel.add(addBtn);
		opPanel.add(subBtn);
		opPanel.add(mulBtn);
		opPanel.add(divBtn);
		opPanel.add(diffBtn1);
		opPanel.add(intBtn1);
		opPanel.add(diffBtn2);
		opPanel.add(intBtn2);
		resPanel.add(resLabel1);
		
		resPanel.add(resField1);
		resPanel.add(resLabel2);
		resPanel.add(resField2);
		mainPanel.add(polyPanel);
		mainPanel.add(new JSeparator());
		mainPanel.add(opPanel);
		mainPanel.add(new JSeparator());
		mainPanel.add(resPanel);

		frame.setContentPane(mainPanel);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		GraphicalUserInterface gui = new GraphicalUserInterface();
		gui.initUI();
	}
	
	private class FieldClicker implements FocusListener {
		JTextField field;
		
		private FieldClicker(JTextField f) {
			field = f;
		}
		public void focusGained(FocusEvent e) {
	        field.selectAll();
	    }

	    public void focusLost(FocusEvent e) {
	    }
	}
	
	private class ParseClicker implements ActionListener {
		JTextField field;
		int poly;
		
		private ParseClicker(JTextField f, int p) {
			field = f;
			poly = p;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			if(poly == 1) {
				poly1 = parsePolynomial(field);
			} else {
				poly2 = parsePolynomial(field);
			}
		}
		
	}
	
	private class OpClicker implements ActionListener {
		String btnText;
		
    	private OpClicker(JButton btn) {
    		btnText = btn.getText();
    	}
		public void actionPerformed(ActionEvent arg0) {
			if(poly1 == null || poly2 == null) {
				JOptionPane.showMessageDialog(frame, "Must input both polynomials first!", "Error!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			switch(btnText)
    		{
    			case "Add":
    				polyResult = Operations.add(poly1, poly2);
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Subtract":
    				polyResult = Operations.subtract(poly1, poly2);
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Multiply":
    				polyResult = Operations.multiply(poly1, poly2);
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Divide":
    				Polynomial[] polyArray = Operations.divide(poly1, poly2);
    				polyResult = polyArray[0];
    				polyRemainder = polyArray[1];
    				resField1.setText(polyResult.toString());
    				resField2.setText(polyRemainder.toString());
    				break;
    			case "Integrate P1":
    				polyResult = poly1.integrate();
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Differentiate P1":
    				polyResult = poly1.differentiate();
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Integrate P2":
    				polyResult = poly2.integrate();
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    			case "Differentiate P2":
    				polyResult = poly2.differentiate();
    				resField1.setText(polyResult.toString());
    				resField2.setText("No remainder for this operation.");
    				break;
    		}
		}    	
	}
	
	private boolean checkInput(String poly) {
    	if(poly.matches(".*[^0-9xX^ +-].*") || poly == "") {
    		JOptionPane.showMessageDialog(frame, "Input is not correct! Polynomial terms mut be written like : [0-9] X^ [0-9] with + or - between them!", "Error!", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	return true;
    }
	
	private Polynomial parsePolynomial(JTextField field) {
		if(!checkInput(field.getText())) {
			return null;
		}
		Polynomial poly = new Polynomial();
		String text = field.getText();
		text = text.replaceAll("\\s", "").replaceAll("-", "+-");
		String[] parts = text.split("\\+");
		deleteEmptyStrings(parts);
		for(String i : parts) {
			System.out.println(i);
			String[] mono = i.split("X\\^");
			deleteEmptyStrings(mono);
			if(mono.length == 2) {
				Monomial aux = new Monomial(Double.parseDouble(mono[0]), Integer.parseInt(mono[1]));
				poly.addMonomial(aux);
			} else {
				Monomial aux = new Monomial(Double.parseDouble(mono[0]), 0);
				poly.addMonomial(aux);
			}
					
		}
		return poly;
	}
	
	private void deleteEmptyStrings(String[] strings) {
		int count = 0;
		for(int i = 0; i < strings.length; i++) {
			if(strings[i] == "") {
				for(int j = i; j < strings.length - 1; j++) {
					strings[j] = strings[j + 1];
				}
			} else count++;
		}
		String[] result = new String[count];
		for(int i = 0; i < count; i++) {
			result[i] = strings[i];
		}
	}
}
