package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import polynomials.Monomial;
import polynomials.Operations;
import polynomials.Polynomial;

class PolyTest {

	@Test
	void testAdd() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Monomial m5 = new Monomial(3,1);
		Monomial m6 = new Monomial(6,0);
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);
		
		p2.addMonomial(m5);
		p2.addMonomial(m6);
		
		assertEquals("7X^5 +12X^3 -4X +10 ",Operations.add(p1, p2).toString());
	}
	
	@Test
	void testSubtract() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Monomial m5 = new Monomial(3,1);
		Monomial m6 = new Monomial(6,0);
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);
		
		p2.addMonomial(m5);
		p2.addMonomial(m6);
		
		assertEquals("7X^5 +12X^3 -10X -2 ",Operations.subtract(p1, p2).toString());
	}
	
	@Test
	void testMultiply() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Monomial m5 = new Monomial(3,1);
		Monomial m6 = new Monomial(6,0);
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);
		
		p2.addMonomial(m5);
		p2.addMonomial(m6);
		
		assertEquals("21X^6 +42X^5 +36X^4 +72X^3 -21X^2 -30X +24 ",Operations.multiply(p1, p2).toString());
	}
	
	@Test
	void testDivide() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Monomial m5 = new Monomial(3,1);
		Monomial m6 = new Monomial(6,0);
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);
		
		p2.addMonomial(m5);
		p2.addMonomial(m6);
		
		Polynomial[] result = Operations.divide(p1, p2);
		
		assertEquals("2.3333333333333335X^4 -4.666666666666667X^3 +13.333333333333334X^2 -26.666666666666668X +51 ",result[0].toString());
		assertEquals("-302 ",result[1].toString());
	}
	
	@Test
	void testIntegrate() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Polynomial p1 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);

		assertEquals("1.1666666666666667X^6 +3X^4 -3.5X^2 +4X ",p1.integrate().toString());
	}
	
	@Test
	void testDifferentiate() {
		Monomial m1 = new Monomial(7,5);
		Monomial m2 = new Monomial(12,3);
		Monomial m3 = new Monomial(-7,1);
		Monomial m4 = new Monomial(4,0);
		
		Polynomial p1 = new Polynomial();
		
		p1.addMonomial(m1);
		p1.addMonomial(m2);
		p1.addMonomial(m3);
		p1.addMonomial(m4);

		assertEquals("35X^4 +36X^2 -7 ",p1.differentiate().toString());
	}
	

}
